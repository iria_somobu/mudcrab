package com.somobu;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.util.*;

public class Utils {

    private static final long DISCORD_EPOCH = 1420070400000L;

    public static String getSnowlfake(long millis){
        return String.valueOf( (millis - DISCORD_EPOCH) << 22 );
    }

    public static long getMillis(String snowflake){
        BigInteger l = new BigInteger(snowflake);

        return l.shiftRight(22).longValue() + DISCORD_EPOCH;
    }


    public static long getNextDayOfWeek(int calendarDayOfWeek) {
        return getNextDayOfWeek(calendarDayOfWeek, 12, 0);
    }

    public static long getNextDayOfWeek(int calendarDayOfWeek, int hour, int minute) {
        Calendar c = Calendar.getInstance();

        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        if (c.get(Calendar.DAY_OF_WEEK) > calendarDayOfWeek) {
            c.add(Calendar.WEEK_OF_MONTH, 1);
        }

        c.set(Calendar.DAY_OF_WEEK, calendarDayOfWeek);

        if (c.getTimeInMillis() < System.currentTimeMillis()) {
            c.add(Calendar.WEEK_OF_MONTH, 1);
        }

        return c.getTimeInMillis();
    }

    public static long getNextDayOfWeekSince(long sinceMillis, int calendarDayOfWeek, int hour, int minute) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(sinceMillis));

        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        if (c.get(Calendar.DAY_OF_WEEK) > calendarDayOfWeek) {
            c.add(Calendar.WEEK_OF_MONTH, 1);
        }

        c.set(Calendar.DAY_OF_WEEK, calendarDayOfWeek);

        if (c.getTimeInMillis() < sinceMillis) {
            c.add(Calendar.WEEK_OF_MONTH, 1);
        }

        return c.getTimeInMillis();
    }

    public static long getNextTimeOfDayAfter(long afterMillis, int hour, int minute) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(afterMillis));

        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        if (c.getTimeInMillis() <= afterMillis) {
            c.add(Calendar.DAY_OF_YEAR, 1);
        }

        return c.getTimeInMillis();
    }

    /**
     * В яве почему-то нет метода PATH. Используя этот костыль мы можем разрешить HttpURLConnection применять указанные
     * нами методы, в том числе и PATCH.
     * <p>
     * Не выдает ошибки на 1.7.0_45 и 1.8.0_212
     * </p><p>
     * 11.0.4 дает предупреждение, что так делать нельзя
     * </p>
     *
     * @param methods - методы, которые хотим разрешить (ex PATCH)
     */
    public static void allowHttpMethod(String... methods) {

        try {

            Field methodsField = HttpURLConnection.class.getDeclaredField("methods");

            Field modifiersFields = Field.class.getDeclaredField("modifiers");
            modifiersFields.setAccessible(true);
            modifiersFields.setInt(methodsField, methodsField.getModifiers() & ~Modifier.FINAL);

            methodsField.setAccessible(true);

            String[] oldFields = (String[]) methodsField.get(null);
            Set<String> methodsSet = new LinkedHashSet<>(Arrays.asList(oldFields));
            methodsSet.addAll(Arrays.asList(methods));
            String[] newMethods = methodsSet.toArray(new String[0]);

            methodsField.set(null, newMethods);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static boolean inArray(String s, String[] array) {
        for (String st : array) {
            if (s == null) {
                if (st == null) {
                    return true;
                }
            } else {
                if (s.equals(st)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static String rand(String... data) {
        int i = (int) (Math.random() * data.length);

        return data[i];
    }
}
