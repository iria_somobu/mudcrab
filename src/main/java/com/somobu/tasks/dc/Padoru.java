package com.somobu.tasks.dc;

import com.somobu.Utils;
import com.iillyyaa2033.discord.v8.objects.Message;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.IOException;
import java.util.Calendar;
import java.util.regex.Pattern;

@SuppressWarnings("unused")
public class Padoru extends DiscordTask {

    public static final String[] padoruPics = {
            "padoru/momiji.png",
            "padoru/momiji.png",
            "padoru/nero.png",
            "padoru/nero2.png",
            "padoru/neptunia.png",
            "padoru/w.png",
            "padoru/bonus.jpg"
    };

    private final Pattern pattern = Pattern.compile(".*падор{1,2}у+.*");

    @Override
    public void onMessage(DiscordConnection connection, DiscordServer server, Message message) throws IOException {
        if (message.content == null) return;

        if (isGlobalPadoruTime() && pattern.matcher(message.content.toLowerCase()).matches()) {
            connection.postPicture(message.channel_id, Utils.rand(padoruPics));
        }
    }

    public static boolean isGlobalPadoruTime() {
        return Calendar.getInstance().get(Calendar.MONTH) == Calendar.DECEMBER;
    }
}
