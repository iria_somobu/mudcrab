package com.somobu.tasks.dc.voice;

import com.iillyyaa2033.discord.v8.objects.*;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordTask;
import com.somobu.kurabu.dcvoice.Playlist;
import com.somobu.kurabu.dcvoice.Providers;
import com.somobu.kurabu.dcvoice.VoiceMgr;

import java.io.IOException;

@SuppressWarnings("unused")
public class Play extends DiscordTask {

    @Override
    public ApplicationCommand getApplicationCommand() {
        ApplicationCommand command = new ApplicationCommand();
        command.name = "play";
        command.description = "Докинуть музыки в плейлист, сменить плейлист";
        command.type = 1;

        ApplicationCommand.Option optionQ = new ApplicationCommand.Option();
        optionQ.type = 3;
        optionQ.required = true;
        optionQ.name = "q";
        optionQ.description = "yt url/hash/search, 'rw'/'rwi'";

        ApplicationCommand.Option optionTop = new ApplicationCommand.Option();
        optionTop.type = 5;
        optionTop.required = false;
        optionTop.name = "top";
        optionTop.description = "Закинуть на самый верх плейлиста";

        command.options = new ApplicationCommand.Option[]{
                optionQ, optionTop
        };

        return command;
    }

    @Override
    public InteractionResponse respondToInteraction(DiscordConnection connection, Interaction event) {

        try {
            Join.join(connection, event);
        } catch (IOException e) {
            return r(e.getMessage());
        }

        VoiceMgr.VoiceWrapper vw = connection.voiceMgr.getVoice(event.guild_id);

        // Error check below is not necessary, but I'll leave it here just to be sure
        if (vw == null) return r("Error: bot is not in a voice channel right now");

        boolean top = false;
        String query = null;

        if (event.data.options != null) {
            for (Interaction.ApplicationCommandInteractionDataOption option : event.data.options) {
                if ("q".equals(option.name)) query = option.value.getAsString();
                else if ("top".equals(option.name)) top = option.value.getAsBoolean();
            }
        }

        if (query != null) {
            try {
                boolean wasEmpty = vw.playlist.entries.size() == 0;

                switch (query) {
                    case "rw":
                        addToPlaylist(vw.playlist, Providers.oneRwPlease(), top);
                        break;
                    case "rwi":
                        synchronized (vw.playlist.entries) {
                            vw.playlist.playlist_manager = Providers.LIST_MGR_RETROWAVE;
                            Providers.manageRW(vw.playlist);
                        }
                        break;
                    default:
                        addToPlaylist(vw.playlist, Providers.youtube(query), top);
                }

                if (wasEmpty) {
                    vw.playNext(false);
                }
            } catch (IOException e) {
                return r("Error: " + e.getMessage());
            }
        }

        return r("Done!");
    }

    private void addToPlaylist(Playlist playlist, Playlist.Entry entry, boolean onTop) {
        synchronized (playlist.entries) {
            if (onTop && playlist.entries.size() > 1) {
                playlist.entries.add(1, entry);
            } else {
                playlist.entries.add(entry);
            }
        }
    }
}
