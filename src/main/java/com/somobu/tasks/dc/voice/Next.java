package com.somobu.tasks.dc.voice;

import com.iillyyaa2033.discord.v8.objects.ApplicationCommand;
import com.iillyyaa2033.discord.v8.objects.Interaction;
import com.iillyyaa2033.discord.v8.objects.InteractionResponse;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordTask;
import com.somobu.kurabu.dcvoice.VoiceMgr;

@SuppressWarnings("unused")
public class Next extends DiscordTask {

    @Override
    public ApplicationCommand getApplicationCommand() {
        ApplicationCommand command = new ApplicationCommand();
        command.name = "next";
        command.description = "Перейти к следующему треку в листе";
        command.type = 1;
        return command;
    }

    @Override
    public InteractionResponse respondToInteraction(DiscordConnection connection, Interaction event) {

        String userVoice = connection.voiceMgr.getChannelOfUser(event.guild_id, event.member.user.id);
        if (userVoice == null) {
            return r("Error: unable to locate user's voice channel");
        }

        VoiceMgr.VoiceWrapper vw = connection.voiceMgr.getVoice(event.guild_id);
        if (vw == null) {
            return r("Error: bot is not in a voice channel right now");
        }

        if (vw.playlist.entries.size() > 0) {
            vw.playNext();
            return r("Skipping to the next track");
        } else {
            return r("It seems that bot is not playing anything");
        }
    }

}
