package com.somobu.tasks.dc.voice;

import com.iillyyaa2033.discord.v8.objects.ApplicationCommand;
import com.iillyyaa2033.discord.v8.objects.Interaction;
import com.iillyyaa2033.discord.v8.objects.InteractionResponse;
import com.somobu.kurabu.Main;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordTask;
import com.somobu.kurabu.dcvoice.VoiceMgr;

import java.io.IOException;

@SuppressWarnings("unused")
public class Join extends DiscordTask {

    @Override
    public ApplicationCommand getApplicationCommand() {
        ApplicationCommand command = new ApplicationCommand();
        command.name = "join";
        command.description = "Саммонит бота в войс";
        command.type = 1;
        return command;
    }

    @Override
    public InteractionResponse respondToInteraction(DiscordConnection connection, Interaction event) {

        String content;

        try {
            content = join(connection, event);
        } catch (IOException e) {
            content = e.getMessage();
        }

        return r(content);
    }

    /**
     * Summon bot to user's voice channel
     *
     * @return success/error message if connected successfully or already in this voice
     * @throws IOException if unable to connect or in another voice channel
     */
    public static String join(DiscordConnection connection, Interaction event) throws IOException {

        String userVoice = connection.voiceMgr.getChannelOfUser(event.guild_id, event.member.user.id);

        if (userVoice == null) throw new IOException("Error: unable to locate user's voice channel");

        VoiceMgr.VoiceWrapper vw = connection.voiceMgr.getVoice(event.guild_id);
        if (vw != null) {
            if (userVoice.equals(vw.channel)) return "Error: bot is already in this voice channel";
            else throw new IOException("Error: bot is already in another voice channel <#" + vw.channel + ">");
        }

        try {
            connection.voiceMgr.join(event.guild_id, userVoice);
            return "Joined voice channel";
        } catch (IOException e) {
            Main.e(e);
            throw new IOException("Failed to join channel: " + e.getMessage());
        }
    }
}
