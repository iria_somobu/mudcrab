package com.somobu.tasks.dc.voice;

import com.somobu.kurabu.dc.DiscordTask;
import com.somobu.kurabu.dcvoice.PlaybackManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

@Deprecated
public class RadioJob extends DiscordTask implements PlaybackManager.HPStateChangeListener {

    private static final String YT_HASH_REGEX = "[a-zA-Z0-9\\-_]{11}";
    private static final String YT_HASH_URL = "https://youtube.com/watch?v=%s";

    private static final String MUSIC_ROOT_KEY = "music_root";
    private static final String CONTROL_CHANNEL = "control_channel";

    private static final long STATE_RATELIMIT = 500; // ms ratelimit for lastActionTime

    private String controlGuildId = null;
    private String controlMessageId = null;

    private String lastAction = null;
    private static volatile long lastActonTime = 0; // Shared across all RadioJob instances
/*
    public String[] names() {
        return new String[]{
                "join", "leave", // TODO: move me to dedicated command
                "p", "skip", "stop",
                "q"
        };
    }

    public void onCommand(String command, Event.NewMessage message) {

        String guild;
        String channel;
        String sourceChannel, sourceMessage;

        if (message.source instanceof Message) {
            Message m = (Message) message.source;
            guild = m.guild_id;
            controlGuildId = m.guild_id;
            sourceChannel = m.channel_id;
            sourceMessage = m.id;

            channel = ((ServiceDC) message.service).users2voice.get(m.author.id);

            if (get(CONTROL_CHANNEL) == null) {
                message.respond("I refuse to play: no control_channel config entry was set.");
                return;
            } else if (!get(CONTROL_CHANNEL).equals(m.channel_id)) {
                message.respond("Рулить войсом можно только из <#" + get(CONTROL_CHANNEL) + ">");
                return;
            }

        } else {
            message.respond("Non-discord radio is not supported for now");
            return;
        }

        if (channel == null) {
            message.respond("Ты в воисе?");
            return;
        }

        if (get(MUSIC_ROOT_KEY) == null) {
            message.respond("I refuse to play: no music_root config entry was set.");
            return;
        }


        dropMessage(sourceChannel, sourceMessage);

        switch (command) {
            case "join":
                join(message, guild, channel);
                break;

            case "leave":
                try {
                    Main.radio.leave(guild, channel);
                    if (controlMessageId != null) {
                        dropMessage(getString(CONTROL_CHANNEL), controlMessageId);
                        controlMessageId = null;
                    }
                } catch (IOException e) {
                    message.respond("Произошла беда при отключении: " + e.getMessage());
                }
                break;

            case "p":
                if (!Main.radio.isInChannel(guild, channel)) join(message, guild, channel);
                if (Main.radio.isInChannel(guild, channel)) {
                    play(message, guild, channel);
                } else {
                    message.respond("Йа ни фканале. Пачиму?");
                }
                break;

            case "q":
                if (Main.radio.isInChannel(guild, channel)) {
                    updateStateMessage();
                } else {
                    message.respond("Я вообще не в войсе, друг");
                }
                break;

            case "skip":
                if (Main.radio.isInChannel(guild, channel)) {
                    Main.radio.playback(guild, channel).skip();
                    if (!Main.radio.playback(guild, channel).isPlaying()) {
                        setStateEvent("Скипнул");
                    }
                } else {
                    message.respond("Как я скипну? Я ж не в воисе");
                }
                break;

            case "stop":
                if (Main.radio.isInChannel(guild, channel)) Main.radio.playback(guild, channel).stop();
                else message.respond("Как я прекращу играть? Чи шо? Я ж не на канале");
                break;
        }
    }

    private void join(Event.NewMessage message, String guild, String channel) {
        if (Main.radio.isInChannel(guild, channel)) {
            message.respond("Уже здесь");
        } else if (!Main.radio.canJoin(guild, channel)) {
            message.respond("Не могу играть в этом канале");
        } else {
            try {
                Main.radio.join(guild, channel);
                Main.radio.playback(guild, channel).setListener(this);
            } catch (IOException e) {
                message.respond("Произошла беда при подключении: " + e.getMessage());
            }
        }
    }

    private void play(Event.NewMessage message, String guild, String channel) {
        String[] args = message.text.split(" ");

        if (args.length < 2) {
            message.respond("Варианты команды:```\n" +
                    "!p yt {URL / keyword}\n" +
                    "!p rw [count] -- закинуть в плейлист ретровейва\n" +
                    "!p file {relative path / keyword}\n" +
                    "!p dir {relative path / keyword}\n" +
                    "!p {YT URL / relative path / filename keyword / YT keyword}" +
                    "```");
            return;
        }

        String lowerTypename = args[1].toLowerCase();
        String subst = message.text.substring(args[0].length() + 1);
        if ("yt".equals(lowerTypename)) {

            if (args.length < 3) {
                message.respond("Правильный синтаксис: !p yt {URL / keyword}");
            } else {
                setStateEvent("Подгружаю музыку с ютуба");
                Main.radio.playback(guild, channel).appendToPlaylist(asYoutube(subst));
                setStateEvent("Enqueued youtube <" + subst + ">");
            }
        } else if ("rw".equals(lowerTypename)) {

            try {
                if (args.length == 3) {
                    try {
                        int cnt = Integer.parseInt(args[2]);
                        if (cnt < 1) cnt = 1;
                        for (RetrowaveApi.TrackMeta track : new RetrowaveApi().nextTracks(cnt)) {
                            Main.radio.playback(guild, channel).appendToPlaylist(
                                    new PlaybackManager.MediaResource(
                                            PlaybackManager.Type.RAW_URL, track.getStreamUrl(), track.title
                                    ));
                        }
                        setStateEvent("Enqueued " + cnt + " retrowave songs");

                    } catch (NumberFormatException e) {
                        setStateEvent("Не удалость распарсить кол-во ретровейва");
                    }
                } else {
                    RetrowaveApi.TrackMeta track = new RetrowaveApi().nextTrack();
                    Main.radio.playback(guild, channel).appendToPlaylist(
                            new PlaybackManager.MediaResource(
                                    PlaybackManager.Type.RAW_URL, track.getStreamUrl(), track.title
                            ));
                }
            } catch (IOException e) {
                setStateEvent("Не удалость выцепить ретровейв");
            }

        } else if ("file".equals(lowerTypename)) {
            message.respond("Unimplemented: file playback");
        } else if ("dir".equals(lowerTypename)) {
            message.respond("Unimplemented: dir playback");
        } else {
            for (PlaybackManager.MediaResource r : wildGuess(subst)) {
                Main.radio.playback(guild, channel).appendToPlaylist(r);
            }
            setStateEvent("Обновил плейлист");
        }
    }

    private PlaybackManager.MediaResource[] wildGuess(String rawText) {
        // TODO: implement file search

        if (rawText.matches("[0-9]{15,19}")) {
            return asDiscordPlaylist(rawText);
        } else {
            return new PlaybackManager.MediaResource[]{asYoutube(rawText)};
        }
    }

    private PlaybackManager.MediaResource[] asDiscordPlaylist(String rawText) {
        String cc = getString(CONTROL_CHANNEL);
        ArrayList<PlaybackManager.MediaResource> mr = new ArrayList<>();

        try {
            Message msg = ServiceDC.api.getChannelMessage(cc, rawText);
            for (String s : msg.content.replace("```", "").split("\n")) {
                String[] a = s.split(" ");

                if (a[0].matches(YT_HASH_REGEX)) {
                    mr.add(new PlaybackManager.MediaResource(
                            PlaybackManager.Type.YOUTUBE_URL,
                            String.format(YT_HASH_URL, a[0]),
                            s.substring(a[0].length() + 1)
                    ));
                }
            }
        } catch (Exception e) {
            onHPSCLError(e.toString());
        }

        return mr.toArray(new PlaybackManager.MediaResource[0]);
    }

    private PlaybackManager.MediaResource asYoutube(String rawText) {
        if (rawText.startsWith("http") || rawText.contains("youtu")) {
            return new PlaybackManager.MediaResource(PlaybackManager.Type.YOUTUBE_URL, rawText);
        }
        if (rawText.matches(YT_HASH_REGEX)) {
            String properUrl = String.format(YT_HASH_URL, rawText);
            return new PlaybackManager.MediaResource(PlaybackManager.Type.YOUTUBE_URL, properUrl);
        } else {
            return new PlaybackManager.MediaResource(PlaybackManager.Type.YOUTUBE_KEYWORD, rawText);
        }
    }

    private String queue(PlaybackManager.MediaResource[] queue) {
        String rz = "";
        if (queue.length < 1) {
            rz += "Очередь пуста";
        } else {
            rz += "Очередь:```";

            for (int i = 0; i < queue.length; i++) {
                rz += "\n" + (i + 1) + ". " + mr2s(queue[i]);

                if (i > 23) {
                    rz += "\nИ еще " + (queue.length - 24);
                    break;
                }
            }

            rz += "```";
        }
        return rz;
    }

    private String mr2s(PlaybackManager.MediaResource r) {
        String rz = "";

        if (r.title == null) {
            switch (r.type) {
                case FILE:
                    rz = r.query.substring(getString(MUSIC_ROOT_KEY).length() + 1);
                    break;
                case YOUTUBE_URL:
                    rz = r.query;
                    break;
                case YOUTUBE_KEYWORD:
                    rz = "YT search: " + r.query;
                    break;
                case RAW_URL:
                    rz = r.query;
            }
        } else {
            rz = r.title;
        }

        return rz;
    }

    private void dropMessage(String channel, String messageId) {
        try {
            ServiceDC.api.deleteMessage(channel, messageId);
        } catch (IOException e) {
            // Ignored
        }
    }
*/
    private void setStateEvent(String event) {
        lastAction = event;

//        updateStateMessage();
    }
/*
    private void updateStateMessage() {

        if (System.currentTimeMillis() - lastActonTime < STATE_RATELIMIT) return;
        lastActonTime = System.currentTimeMillis();

        try {
            String cc = getString(CONTROL_CHANNEL);
            String currentPlaying;
            String queue = queue(Main.radio.playback(controlGuildId, cc).queue());

            PlaybackManager.MediaResource current = Main.radio.playback(controlGuildId, cc).current();
            if (current == null) currentPlaying = "Сейчас ничего не играю";
            else currentPlaying = "Сейчас: `" + mr2s(current) + "`";

            Embed embed = new Embed();
            embed.color = (int) (Math.random() * 16777216);
            embed.title = currentPlaying;
            embed.description = queue;
            embed.footer = new EmbedFooter();
            embed.footer.text = lastAction;
            embed.timestamp = getISO8601(lastActonTime);

            boolean forceCreate = false;
            if (controlMessageId != null) {
                try {
                    ServiceDC.api.editMessage(cc, controlMessageId, "", embed, 0, null);
                } catch (IOException e) {
                    forceCreate = true;
                }
            }

            if (forceCreate || controlMessageId == null) {
                MessageJson json = new MessageJson();
                json.embed = embed;
                try {
                    Message msg = ServiceDC.api.createMessage(cc, json);
                    controlMessageId = msg.id;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            // TODO: what should I do here?
        }
    } */

    @Override
    public void onHPSCLNext() {
        setStateEvent("Перешел к следующей строчке");
    }

    @Override
    public void onHPSCLNone() {
        setStateEvent("Закончил играть");
    }

    @Override
    public void onHPSCLError(String error) {
        setStateEvent("Ошибка: " + error);
    }

    private static String getISO8601(long millis) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(new Date(millis));
    }
}
