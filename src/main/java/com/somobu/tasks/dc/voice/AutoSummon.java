package com.somobu.tasks.dc.voice;

import com.iillyyaa2033.discord.v8.objects.VoiceState;
import com.somobu.kurabu.Main;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;
import com.somobu.kurabu.dcvoice.Providers;
import com.somobu.kurabu.dcvoice.VoiceMgr;

import java.io.IOException;
import java.util.List;

@SuppressWarnings("unused")
public class AutoSummon extends DiscordTask {

    private int wasInVoice = 0;

    @Override
    public void onVoiceStateUpdate(DiscordConnection connection, DiscordServer server, VoiceState state) {

        String targetChannel = getString("voice");
        if (targetChannel == null) return;

        VoiceMgr.VoiceWrapper vw = connection.voiceMgr.getVoice(state.guild_id);
        if (vw != null && !vw.channel.equals(targetChannel)) return;

        boolean inTargetChannel = vw != null;
        List<String> users = connection.voiceMgr.getUsersInVoice(targetChannel);

        int nowInVoice = users.size();

        if (!inTargetChannel && wasInVoice == 0 && nowInVoice > 0) {
            try {
                // This line is needed to prevent bug:
                // When bot killed and restarted in a short time (<15m) discord thinks that bot is still in voice
                // this leads to bot unable to `join` the same voice channel
                connection.voiceMgr.leave(server.id);

                VoiceMgr.VoiceWrapper voice = connection.voiceMgr.join(state.guild_id, targetChannel);
                voice.playlist.playlist_manager = Providers.LIST_MGR_RETROWAVE;
                voice.playNext();
            } catch (IOException e) {
                Main.e(e);
            }
        }

        if (inTargetChannel && wasInVoice > 1 && nowInVoice <= 1) {
            connection.voiceMgr.leave(server.id);
        }

        wasInVoice = users.size();
    }
}
