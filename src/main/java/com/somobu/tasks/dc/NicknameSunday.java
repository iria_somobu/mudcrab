package com.somobu.tasks.dc;

import com.somobu.Utils;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.IOException;
import java.util.Calendar;

@SuppressWarnings("unused")
public class NicknameSunday extends DiscordTask {

    @Override
    public long getLaunchTimeAfter(DiscordConnection connection, DiscordServer server, long millis) {
        return Utils.getNextDayOfWeekSince(millis, Calendar.SUNDAY, 23, 50);
    }

    @Override
    public void onScheduled(DiscordConnection connection, DiscordServer server) throws IOException {
        connection.api.modifyCurrentUserNick(server.id, Utils.rand(
                "Грязекраб",
                "Грязекрабик",
                "Грязекрабчик",
                "Грязекрабище",
                "М-р Краббс",
                "Крабе"
        ));

/*        Main.setGameStatus(Utils.rand(
                "TES III: Morrowind",
                "TES V: Skyrim",
                "Zuma's revenge",
                "Mass effect",
                "Bioshock",
                "Cryzis",
                "Assasin's creed",
                "Portal",
                "Half-life: Episode Two",
                "Halo 3",
                "Team fortress 2",
                "Uncharted: Drake's fortune",
                "The darkness"
        )); */
    }
}
