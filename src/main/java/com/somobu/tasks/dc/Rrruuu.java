package com.somobu.tasks.dc;

import com.somobu.Utils;
import com.iillyyaa2033.discord.v8.objects.Message;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.IOException;
import java.util.regex.Pattern;

@SuppressWarnings("unused")
public class Rrruuu extends DiscordTask {

    public static String[] rrruuuPics = {
            "rrruuu/koshi-02.png",
            "rrruuu/koshi-03.png",
            "rrruuu/koshi-06.png",
            "rrruuu/koshi-41.png"
    };

    private static final Pattern pattern = Pattern.compile(".*ррр+ууу+.*");

    @Override
    public void onMessage(DiscordConnection connection, DiscordServer server, Message message) throws IOException {
        if (message.content == null) return;

        boolean isRrruuu = pattern.matcher(message.content.toLowerCase()).matches();
        if (isRrruuu) {

            if (Padoru.isGlobalPadoruTime()) { // В декабре постим с 5% шансом падору
                if (Math.random() < 0.05) connection.postPicture(message.channel_id, Utils.rand(Padoru.padoruPics));
                else connection.postPicture(message.channel_id, Utils.rand((rrruuuPics)));
            } else {
                connection.postPicture(message.channel_id, Utils.rand((rrruuuPics)));
            }
        }

    }
}
