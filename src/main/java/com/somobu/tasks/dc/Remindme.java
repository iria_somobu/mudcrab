package com.somobu.tasks.dc;

import com.iillyyaa2033.discord.v8.objects.*;
import com.somobu.kurabu.DB;
import com.somobu.kurabu.Main;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;
import com.somobu.kurabu.Log;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Remindme extends DiscordTask {

    @Override
    public long getLaunchTimeAfter(DiscordConnection connection, DiscordServer server, long millis) {
        try {
            DB.Reminder reminder = Main.db.getNearestReminder(server.id, millis);
            if (reminder != null) {
                return reminder.millis;
            }
        } catch (SQLException e) {
            Main.e(e);
        }

        return millis + 60 * 60 * 1000;
    }

    @Override
    public void onScheduled(DiscordConnection connection, DiscordServer server) throws IOException {
        long current_time = System.currentTimeMillis();
        try {
            while (true) {
                DB.Reminder reminder = Main.db.getNearestReminder(server.id, 0);

                if (reminder == null) return;
                if (reminder.millis < current_time + 1000) {
                    doReminder(connection, reminder);
                    Main.db.removeReminder(reminder.id);
                } else {
                    return;
                }
            }
        } catch (SQLException throwables) {
            Log.e(throwables);
        }
    }

    private void doReminder(DiscordConnection connection, DB.Reminder reminder) {
        try {
            connection.postMessage(
                    reminder.channel,
                    "<@" + reminder.user_id + "> напоминалка:\n" + reminder.message_txt
            );
        } catch (IOException e) {
            Log.e(e);
        }
    }

    @Override
    public ApplicationCommand getApplicationCommand() {

        ApplicationCommand command = new ApplicationCommand();
        command.name = "remind";
        command.description = "Напоминалка";
        command.type = 1;

        command.options = new ApplicationCommand.Option[]{
                stringOption(),
                intOption("minutes", "Минуты"),
                intOption("hours", "Часы"),
                intOption("days", "Дни"),
                intOption("months", "Месяцы"),
                intOption("years", "Годы")
        };

        return command;
    }

    private ApplicationCommand.Option stringOption() {
        ApplicationCommand.Option option = new ApplicationCommand.Option();
        option.type = 3;
        option.required = false;
        option.name = "text";
        option.description = "Текст напоминалки";
        return option;
    }

    private ApplicationCommand.Option intOption(String name, String descr) {
        ApplicationCommand.Option option = new ApplicationCommand.Option();
        option.type = 4;
        option.required = false;
        option.name = name;
        option.description = descr;
        return option;
    }


    @Override
    public InteractionResponse respondToInteraction(DiscordConnection connection, Interaction event) {

        String note = "";
        Calendar calendar = new GregorianCalendar();

        if (event.data.options != null) {
            for (Interaction.ApplicationCommandInteractionDataOption option : event.data.options) {
                switch (option.name) {
                    case "text":
                        note = option.value.getAsString();
                        break;
                    case "minutes":
                        calendar.add(Calendar.MINUTE, option.value.getAsInt());
                        break;
                    case "hours":
                        calendar.add(Calendar.HOUR, option.value.getAsInt());
                        break;
                    case "days":
                        calendar.add(Calendar.DAY_OF_YEAR, option.value.getAsInt());
                        break;
                    case "months":
                        calendar.add(Calendar.MONTH, option.value.getAsInt());
                        break;
                    case "years":
                        calendar.add(Calendar.YEAR, option.value.getAsInt());
                        break;
                }
            }
        }

        String date = new SimpleDateFormat("HH:mm dd.MM.yyyy").format(calendar.getTime());
        String msg;
        try {
            String uid = null;
            if (event.user != null) uid = event.user.id;
            else if (event.member != null && event.member.user != null) uid = event.member.user.id;

            Main.db.addReminder(calendar.getTimeInMillis(), event.guild_id, event.channel_id, uid, note);
            Main.necron.notifyTaskRecalc();
            msg = "Поставил напоминалку на " + date;
        } catch (SQLException throwables) {
            msg = "Не удалось забить напоминалку в базу";
            Log.e(throwables);
        }

        InteractionResponse response = new InteractionResponse();
        response.type = 4;
        response.data = new InteractionCallbackData();
        response.data.content = msg;

        return response;
    }
}
