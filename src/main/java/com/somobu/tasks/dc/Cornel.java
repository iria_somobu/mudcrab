package com.somobu.tasks.dc;

import com.somobu.Utils;
import com.iillyyaa2033.discord.v8.objects.Message;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.IOException;

@SuppressWarnings("unused")
public class Cornel extends DiscordTask {

    /**
     * Айдишник Корнела
     */
    public static final String CORNEL = "418425332107051009";

    /**
     * Сколько букв от Корнела достаточно, чтобы считать его сообщение проявлением невероятной воли
     */
    public static final long CHARS_CORNEL = 750;

    /**
     * Сколько букв от любого человека достаточно, чтобы считать его сообщение проявлением невероятной воли
     */
    public static final long CHARS_NON_CORNEL = 1200;

    @Override
    public void onMessage(DiscordConnection connection, DiscordServer server, Message message) throws IOException {
        if (message.author == null || message.content == null) return;

        String author = message.author.id;
        if (CORNEL.equals(author) && message.content.length() > CHARS_CORNEL) {
            connection.postMessage(message.channel_id, Utils.rand(
                    "Великолепная воля, <@" + author + ">, впечатляющее достоинство!",
                    "Невероятная воля, <@" + author + ">, впечатляющее достоинство.",
                    "Впечатляющая воля, <@" + author + ">, невероятное достоинство."
            ));
        } else if (message.content.length() > CHARS_NON_CORNEL) {
            connection.postMessage(message.channel_id, Utils.rand(
                    "Неплохо, очень неплохо, <@" + author + ">. Ваша воля великолепна!",
                    "Великолепная воля, <@" + author + ">",
                    "Впечатляющее достоинство, <@" + author + ">"
            ));
        }
    }
}
