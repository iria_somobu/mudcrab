package com.somobu.tasks.dc;

import com.google.gson.*;
import com.iillyyaa2033.discord.v8.DiscordHttp;
import com.iillyyaa2033.discord.v8.objects.*;
import com.somobu.Utils;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.*;
import java.lang.reflect.Type;
import java.util.*;

/**
 * Dumps channel's contents to disk. Can only be used manually by somobu.
 */
@SuppressWarnings("unused")
public class ChDump extends DiscordTask {

    static final boolean LOG_CHANNEL_DUMPER = false;

    @Override
    public ApplicationCommand getApplicationCommand() {
        ApplicationCommand command = new ApplicationCommand();
        command.name = "dump";
        command.description = "Dump current guild's channels to disk";
        command.type = 1;
        return command;
    }

    @Override
    public InteractionResponse respondToInteraction(DiscordConnection connection, Interaction event) {

        String content;
        if (!"364795733158920193".equals(event.member.user.id)) {
            content = "You're not a Somobu to do this!";
        } else {
            content = "Dump started in background. Watch log for details";
            new FetcherThread(connection.api, event.guild_id).start();
        }

        InteractionResponse response = new InteractionResponse();
        response.type = 4;
        response.data = new InteractionCallbackData();
        response.data.content = content;
        response.data.flags = 1 << 6;

        return response;
    }

    static class BooleanAdapter implements JsonSerializer<Boolean> {
        @Override
        public JsonElement serialize(Boolean src, Type typeOfSrc, JsonSerializationContext context) {
            if (!src) return null;
            return new JsonPrimitive(src);
        }
    }

    static class IntAdapter implements JsonSerializer<Integer> {
        @Override
        public JsonElement serialize(Integer src, Type typeOfSrc, JsonSerializationContext context) {
            if (src == 0) return null;
            return new JsonPrimitive(src);
        }
    }

    static class LongAdapter implements JsonSerializer<Long> {
        @Override
        public JsonElement serialize(Long src, Type typeOfSrc, JsonSerializationContext context) {
            if (src == 0) return null;
            return new JsonPrimitive(src);
        }
    }

    static class StringAdapter implements JsonSerializer<String> {
        @Override
        public JsonElement serialize(String src, Type typeOfSrc, JsonSerializationContext context) {
            if (src == null || src.trim().isEmpty()) return null;
            return new JsonPrimitive(src);
        }
    }

    static class Data {
        HashMap<String, User> authors = new HashMap<>();
        ArrayList<Message> messages = new ArrayList<>();
        HashMap<String, String> m2a = new HashMap<>();
    }

    static class FetcherThread extends Thread {

        DiscordHttp api;
        String guild;

        FetcherThread(DiscordHttp api, String guild) {
            this.api = api;
            this.guild = guild;
        }

        @Override
        public void run() {
            try {
                System.out.println("Guild dumper: started");
                for (Channel ch : api.getGuildChannels(guild)) {
                    System.out.println("Guild dumper: dumping "+ch.name);
                    dumpChannel(ch);
                    sleep(5 * 1000);
                }
                System.out.println("Guild dumper: finished");
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }

        void dumpChannel(Channel channelObject) {
            String channel = channelObject.id;
            File target = new File("dumps/channels/" + channel + ".json");
            File pnt = target.getParentFile();
            if (!pnt.exists() && !pnt.mkdirs()) {
                System.out.println("Unable to make dirs for " + target.getParentFile().getAbsolutePath());
                return;
            }

            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Boolean.class, new BooleanAdapter())
                    .registerTypeAdapter(Integer.class, new IntAdapter())
                    .registerTypeAdapter(Long.class, new LongAdapter())
                    .registerTypeAdapter(String.class, new StringAdapter())
                    .setPrettyPrinting()
                    .create();

            Data data = new Data();
            String after = "0";

            try (Reader reader = new FileReader(target)) {
                data = gson.fromJson(reader, Data.class);
                after = findMax(after, data.messages.toArray(new Message[0]));
                if (LOG_CHANNEL_DUMPER) System.out.println("ChD: Restored " + data.messages.size() + " messages");
            } catch (Exception ignored) {

            }

            try {
                while (true) {
                    if (LOG_CHANNEL_DUMPER) System.out.println("ChD: Requesting messages after " + after);

                    Message[] msgs = {};
                    for (int i = 5; i >= 0; i--) {
                        try {
                            msgs = api.getChannelMessages(channel, null, null, after, 100);
                            break;
                        } catch (Exception e) {
                            System.out.println("Got error " + e.getMessage());
                            if (i == 0) throw e;
                            else sleep(5 * 1000);
                        }
                    }

                    if (LOG_CHANNEL_DUMPER) System.out.println("ChD: Got " + msgs.length + " messages");

                    for (Message message : msgs) {
                        data.m2a.put(message.id, message.author.id);
                        data.authors.put(message.author.id, message.author);
                        message.author = null;

                        data.messages.add(message);
                    }

                    if (LOG_CHANNEL_DUMPER) System.out.println("ChD: Now we have " + data.messages.size() + " messages");

                    if (msgs.length < 1) break;

                    after = findMax(after, msgs);

                    if (LOG_CHANNEL_DUMPER) System.out.println("ChD: Going to sleep a bit");
                    sleep(5 * 1000);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            Collections.sort(data.messages, new Comparator<Message>() {
                @Override
                public int compare(Message message, Message t1) {
                    return message.timestamp.compareTo(t1.timestamp);
                }
            });

            for (Message m : data.messages) {
                m.channel_id = null;
                if (m.mentions != null && m.mentions.length == 0) m.mentions = null;
                if (m.mention_roles != null && m.mention_roles.length == 0) m.mention_roles = null;
                if (m.attachments != null && m.attachments.length == 0) m.attachments = null;
                if (m.embeds != null && m.embeds.length == 0) m.embeds = null;
            }

            try (Writer writer = new FileWriter(target)) {
                gson.toJson(data, writer);
                if (LOG_CHANNEL_DUMPER) System.out.println("ChD: Dumped!");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        String findMax(String after, Message[] msgs) {
            long maxTime = after == null ? 0 : Utils.getMillis(after);
            String maxFlake = after;

            for (Message msg : msgs) {
                long time = Utils.getMillis(msg.id);
                if (time > maxTime) {
                    maxTime = time;
                    maxFlake = msg.id;
                }
            }

            return maxFlake;
        }
    }
}
