package com.somobu.tasks.dc;

import com.somobu.Utils;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.IOException;
import java.util.Calendar;

@SuppressWarnings("unused")
public class DudesWednesday extends DiscordTask {

    @Override
    public long getLaunchTimeAfter(DiscordConnection connection, DiscordServer server, long millis) {
        return Utils.getNextDayOfWeekSince(millis, Calendar.WEDNESDAY, 9, 0);
    }

    @Override
    public void onScheduled(DiscordConnection connection, DiscordServer server) throws IOException {
        connection.postPicture(server.main_channel, Utils.rand(
                "dudes/dudes_wednesday.jpg",
                "dudes/dudes_wednesday_2.png",
                "dudes/dudes_wednesday_3.jpeg",
                "dudes/dudes_wednesday_own.png"
        ));
    }
}
