package com.somobu.tasks.dc;

import com.iillyyaa2033.discord.v8.objects.Message;
import com.somobu.kurabu.Main;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;

@SuppressWarnings("unused")
public class MirrorToIrc extends DiscordTask {

    @Override
    public void onMessage(DiscordConnection connection, DiscordServer config, Message message) {
        if (message.channel_id.equals(getString("from_discord_channel"))) {
            if (Main.irc != null) {
                Main.irc.bot.sendMessage(getString("to_irc_channel"), message.author.username + ": " + message.content);
            }
        }
    }
}
