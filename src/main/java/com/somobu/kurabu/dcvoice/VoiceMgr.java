package com.somobu.kurabu.dcvoice;

import com.iillyyaa2033.discord.voicev4.DiscordApiVoice;
import com.iillyyaa2033.discord.voicev4.IPlaybackListener;
import com.iillyyaa2033.discord.voicev4.IVoiceListener;
import com.iillyyaa2033.discord.voicev4.objects.VoiceSpeaking;
import com.somobu.kurabu.Log;
import com.somobu.kurabu.Main;
import com.somobu.kurabu.dc.DiscordConnection;

import javax.sound.sampled.AudioInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VoiceMgr {

    private static final boolean DEBUG = true;

    private final DiscordConnection connection;

    /**
     * Мапа "сервер+юзер" -> "айдишник войса"
     */
    private final Map<String, String> users2voice = new HashMap<>();

    /**
     * Мапа "сервер" -> "контрол воиса"
     */
    private final Map<String, VoiceWrapper> guilds2voice = new HashMap<>();


    public VoiceMgr(DiscordConnection connection) {
        this.connection = connection;
    }


    public void setVoiceOfUser(String guild, String userid, String channel) {
        users2voice.put(guild + ":" + userid, channel);

        if (connection.user.id.equals(userid)) {
            VoiceWrapper vw = getVoice(guild);
        }

    }

    public String getChannelOfUser(String guild, String user) {
        return users2voice.get(guild + ":" + user);
    }

    public List<String> getUsersInVoice(String channel) {
        List<String> rz = new ArrayList<>();
        for (Map.Entry<String, String> pair : users2voice.entrySet()) {
            if (channel.equals(pair.getValue())) rz.add(pair.getKey());
        }
        return rz;
    }


    public VoiceWrapper getVoice(String guild) {
        return guilds2voice.get(guild);
    }

    public VoiceWrapper join(String guild, String channel) throws IOException {
        leave(guild);

        VoiceWrapper vw = new VoiceWrapper(guild, channel);
        try {
            vw.voice.connect(connection.realtime, guild, channel);
        } catch (IOException e) {
            if (e.getMessage().equals("Websocket is closing")) {
                // TODO: Do nothing here?
            } else {
                throw e;
            }
        }

        guilds2voice.put(guild, vw);

        return vw;
    }

    public void leave(String guild) {
        VoiceWrapper old = getVoice(guild);

        if (old != null && old.voice != null) {
            old.voice.disconnect(connection.realtime);
            old.voice = null;
        }

        guilds2voice.remove(guild);
    }

    public class VoiceWrapper {
        public final String guild;
        public final String channel;
        public final Playlist playlist = new Playlist();

        volatile DiscordApiVoice voice;

        private final MediaConverter converter = new MediaConverter();
        private long currentlyPlaying = System.currentTimeMillis();

        VoiceWrapper(final String guild, String channel) {
            this.guild = guild;
            this.channel = channel;

            voice = new DiscordApiVoice(new IVoiceListener() {

                @Override
                public void onVoiceReady() {
                    if (DEBUG) System.out.println("On voice ready");
                }

                @Override
                public void onError(Exception e) {
                    if (DEBUG) System.out.println("On voice error " + e.getMessage());
                }

                @Override
                public void onClose(boolean b, int i, String s) {
                    if (DEBUG) Log.d("Voice socket is closing: " + b + " " + i + " " + s);
                    // We may be force-moved to another channel
                    stopPlaying();
                    leave(guild);
                }

                @Override
                public void onSpeaking(VoiceSpeaking voiceSpeaking) {

                }

                @Override
                public void onClientDisconnect(String s) {
                    if (DEBUG) System.out.println("On voice client disconnect " + s);
                }
            });
        }

        public void playNext() {
            playNext(true);
        }

        public void playNext(boolean removeCurrent) {
            try {
                if (voice != null) {
                    currentlyPlaying = System.currentTimeMillis();
                    converter.dropAll();

                    Playlist.Entry nextEntry = null;
                    synchronized (playlist.entries) {
                        if (playlist.entries.size() > 0 && removeCurrent) {
                            playlist.entries.remove(0);
                        }

                        Providers.managePlaylist(playlist);

                        if (playlist.entries.size() > 0) {
                            nextEntry = playlist.entries.get(0);
                        }
                    }

                    if (nextEntry != null) {
                        AudioInputStream next = Providers.launchConverter(nextEntry, converter);
                        play(next, currentlyPlaying);
                        if (DEBUG) System.out.println("Successfully scheduled next track");
                    } else if (playlist.entries.size() > 0) {
                        playNext();
                    } else {
                        if (DEBUG) System.out.println("Playlist is empty -- nothing to play");
                    }

                }
            } catch (Exception e) {
                Main.e(e);
            }
        }

        public void stopPlaying() {
            voice.stopPlayback();
            converter.dropAll();
        }

        private void play(AudioInputStream stream, final long tag) throws IOException {
            if (stream == null) throw new IllegalStateException("You should provide non-NULL stream!");

            voice.startPlayback(stream, new IPlaybackListener() {
                @Override
                public void onFinished() {
                    if (currentlyPlaying == tag) {
                        if (DEBUG) Main.d("Voice playback finished");
                        converter.dropAll();
                        playNext();
                    }
                }

                @Override
                public void onError(Exception e) {
                    if (currentlyPlaying == tag) {
                        if (DEBUG) Main.d("Playback error");
                        Main.e(e);
                        converter.dropAll();
                    }
                }
            });
        }
    }

}
