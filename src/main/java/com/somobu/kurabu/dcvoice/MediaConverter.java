package com.somobu.kurabu.dcvoice;

import com.iillyyaa2033.utils.StreamUtils;
import com.somobu.kurabu.Log;
import com.somobu.kurabu.Main;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.*;

public class MediaConverter {

    public interface Listener {
        void onMediaConversionFinished(int exitCode);

        void onMediaConversionError(Exception e);
    }

    private static final boolean DEBUG_BASH = false;
    private static final boolean DEBUG_CONVERTER = false;

    private static final String FFMPEG_OUTPUT = "-vn -ar 48000 -ac 2 -f wav";
    private static final String FFMPEG_QUIET = "-hide_banner -loglevel error";

    private final Listener listener;
    private Process ffmpegProcess = null;

    public MediaConverter() {
        this(new Listener() {

            @Override
            public void onMediaConversionFinished(int exitCode) {
                if (DEBUG_CONVERTER) Main.d("Media conversion finished: " + exitCode);
            }

            @Override
            public void onMediaConversionError(Exception e) {
                if (DEBUG_CONVERTER) Main.d("Media conversion error");
                Main.e(e);
            }

        });
    }

    public MediaConverter(Listener listener) {
        this.listener = listener;
    }

    public AudioInputStream launchFfmpegFileConverter(String source) throws IOException, InterruptedException {
        File o = getPipeFile();

        String destination = o.getAbsolutePath();
        String fmt = "ffmpeg -y -i %s " + FFMPEG_OUTPUT + " \"%s\" " + FFMPEG_QUIET;
        runFfmpegSubprocess(String.format(fmt, source, destination), destination);

        return getAudioStream(o);
    }

    public AudioInputStream launchYTDLtoFfmpegToConverter(String url) throws IOException, InterruptedException {
        File o = getPipeFile();

        String destination = o.getAbsolutePath();
        String fmt = "youtube-dl -i -q -f bestaudio -o - \"%s\"" +
                " | ffmpeg -y -i pipe: " + FFMPEG_OUTPUT + " \"%s\" " + FFMPEG_QUIET
                + " > /dev/null 2>&1"; // Ignore everything from stdout & stderr
        runFfmpegSubprocess(String.format(fmt, url, destination), destination);

        try {
            return getAudioStream(o);
        } catch (Exception e) {
            dropFfmpeg();
            dropPipeFile(o);
            throw new RuntimeException(e);
        }
    }

    private File getMyName() {
        return new File("/tmp/mudcrab_pipe_" + this.hashCode() + ".wav");
    }

    private File getPipeFile() throws IOException, InterruptedException {
        File f = getMyName();
        String af = f.getAbsolutePath();

        dropPipeFile(f);

        int rz = bashExec("mkfifo " + af).waitFor();
        if (rz != 0) throw new IOException("Unable to create named pipe!");

        return f;
    }

    /**
     * You cannot just use `AudioInputStream getAudioInputStream(File file)` on a WAV-file -- you'll get exception
     * like `Resetting to invalid mark`.
     * <p>
     * Instead, you have to write FileInputStream to BufferedInputStream and only then pass it to AudioSystem.
     * </p><p>
     * <a href="https://stackoverflow.com/questions/5529754#5529906">SO link</a>
     * </p>
     */
    private AudioInputStream getAudioStream(File o) {

        Exception caught = null;
        for (int i = 0; i < 5; i++) {
            try {
                FileInputStream fis = new FileInputStream(o);
                InputStream bufferedIn = new BufferedInputStream(fis);
                return AudioSystem.getAudioInputStream(bufferedIn);
            } catch (Exception e) {
                caught = e;
            }
        }

        throw new RuntimeException("Unable to get audio stream after 5 iterations", caught);
    }


    private synchronized void runFfmpegSubprocess(final String bash, final String pipe) {
        dropFfmpeg();

        new Thread() {

            @Override
            public void run() {
                try {
                    String guard = "{ %s ; } || { echo -n > \"%2$s\" ; rm \"%2$s\" ; }";
                    Process p = bashExec(String.format(guard, bash, pipe));
                    ffmpegProcess = p;

                    int rz = p.waitFor();
                    listener.onMediaConversionFinished(rz);
                    if (rz != 0) {
                        if (rz == 143) { // Why 143?! Assumption: force-close exit code
                            if (DEBUG_BASH) Log.d("ffmpeg subprocess ended with error code 143");
                        } else {
                            Log.e(new IOException("Err " + rz + " | " + StreamUtils.convertStreamToString(p.getErrorStream())));
                        }
                    }
                } catch (Exception e) {
                    listener.onMediaConversionError(e);
                }
            }
        }.start();

    }

    private Process bashExec(String bash) throws IOException {
        if (DEBUG_BASH) Main.d("Bash exec: |" + bash + "|");
        return Runtime.getRuntime().exec(new String[]{"/bin/bash", "-e", "-c", bash});
    }

    public void dropAll() {
        try {
            dropFfmpeg();
            dropPipeFile(getMyName());
        } catch (IOException | InterruptedException e) {
            Main.e(e);
        }
    }

    synchronized void dropFfmpeg() {
        if (ffmpegProcess != null) {
            try {
                bashExec("kill -2 $(ps -eo pid,args | grep \"" + getMyName().getAbsolutePath() + "\" | head -n 1 | awk '{print $1}')");
            } catch (IOException e) {
                Main.e(e);
            }
            ffmpegProcess.destroy();
            ffmpegProcess = null;
        }
    }

    synchronized void dropPipeFile(File o) throws IOException, InterruptedException {
        int rz = bashExec("rm -f " + o.getAbsolutePath()).waitFor();
        if (rz != 0) throw new IOException("Unable to remove old named pipe!");
    }

}
