package com.somobu.kurabu.dcvoice;

import com.iillyyaa2033.discord.voicev4.DiscordApiVoice;
import com.iillyyaa2033.discord.voicev4.IPlaybackListener;
import com.iillyyaa2033.discord.voicev4.IVoiceListener;
import com.iillyyaa2033.discord.voicev4.objects.VoiceSpeaking;
import com.iillyyaa2033.utils.StreamUtils;
import com.somobu.kurabu.Log;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Deprecated
public class PlaybackManager {

    private HashMap<String, HardcodedPlayback> playbacks = new HashMap<>();

    public enum Type {
        FILE,
        YOUTUBE_URL,
        YOUTUBE_KEYWORD,
        RAW_URL
    }

    public static class MediaResource {

        public final String query;
        public final Type type;
        public final String title;

        public MediaResource(Type type, String query, String title) {
            this.type = type;
            this.query = query;
            this.title = title;
        }
    }

    public interface HPStateChangeListener {

        void onHPSCLNext();

        void onHPSCLNone();

        void onHPSCLError(String error);

    }

    public class HardcodedPlayback implements IPlaybackListener {

        final String guild, voice_channel;

        private final DiscordApiVoice voice;
        private boolean playing = false;

        private MediaResource playbackCurrent = null; // Не доверяй этой переменной - код может не обновить ее properly
        private final List<MediaResource> playbackQueue = new ArrayList<>();
        private Process ffmpegProcess = null;

        private HPStateChangeListener listener = null;

        HardcodedPlayback(final String guild, String channel) throws IOException {
            this.guild = guild;
            this.voice_channel = channel;
            voice = new DiscordApiVoice(new IVoiceListener() {

                @Override
                public void onVoiceReady() {

                }

                @Override
                public void onError(Exception ex) {
                    ex.printStackTrace();
                }

                @Override
                public void onClose(boolean b, int i, String s) {
                    markNotPlaying();
                    playbacks.remove(guild);
                }

                @Override
                public void onSpeaking(VoiceSpeaking speaking) {

                }

                @Override
                public void onClientDisconnect(String s) {

                }
            });
//            voice.connect(Main.deprecatedDiscord.realtime, guild, channel, false, true);
        }

        private void goNext() {
            try {
                if (playbackQueue.size() > 0) {
                    MediaResource f = playbackQueue.remove(0);
                    playbackCurrent = f;

                    AudioInputStream stream = spawnConverter(f);

                    markNotPlaying();
                    voice.stopPlayback();
                    voice.startPlayback(stream, this);
                    markPlaying();

                    if (listener != null) listener.onHPSCLNext(); // Should be called strictly after `markPlaying()`
                } else {
                    if (listener != null) listener.onHPSCLNone();
                }
            } catch (FileNotFoundException ignored) {
                // Happens when playback went wrong, ignore it for now
            } catch (Exception e) {
                onPlaybackError(e);
            }
        }

        private void onPlaybackError(Exception e) {
            if (listener != null) listener.onHPSCLError(e.getMessage() + " " + e.getStackTrace()[0].toString());
        }

        private void markPlaying() {
            playing = true;
        }

        private void markNotPlaying() {
            playing = false;
        }


        // Discord playback listener

        @Override
        public void onError(Exception e) {
            onPlaybackError(e);
            markNotPlaying();
        }

        @Override
        public void onFinished() {
            if (isPlaying()) {
                markNotPlaying();
                goNext();
            }
        }


        // Playlist

        private synchronized AudioInputStream spawnConverter(MediaResource f) throws Exception {
            File o = new File("/tmp/mudcrab_pipe_" + this.hashCode() + ".wav");

            makeNamedPipe(o);

            if (f.type == Type.FILE) {
                runFfmpegFileConverter(new File(f.query).getAbsolutePath(), o);
            } else if (f.type == Type.YOUTUBE_URL) {
                runYTDLtoFfmpegToConverter(f.query, o);
            } else if (f.type == Type.YOUTUBE_KEYWORD) {
                runYTDLtoFfmpegToConverter("ytsearch1:" + f.query, o);
            } else if (f.type == Type.RAW_URL) {
                runFfmpegFileConverter(f.query, o);
            }

            Exception caught = null;
            for (int i = 0; i < 5; i++) {
                try {
                    return getProperAudioInputStream(o);
                } catch (Exception e) {
                    caught = e;
                }
            }

            throw new RuntimeException("Unable to get audio stream after 5 iterations", caught);
        }

        private void makeNamedPipe(File f) throws IOException, InterruptedException {
            String af = f.getAbsolutePath().replace(" ", "\\ ");

            int rz = Runtime.getRuntime().exec("rm -f " + af).waitFor();
            if (rz != 0) throw new IOException("Unable to remove old named pipe!");

            rz = Runtime.getRuntime().exec("mkfifo " + af).waitFor();
            if (rz != 0) throw new IOException("Unable to create named pipe!");
        }

        private static final String FFMPEG_QUIET = "-hide_banner -loglevel error";

        private void runFfmpegFileConverter(String source, File o) {
            String destination = o.getAbsolutePath();

            String fmt = "ffmpeg -y -i %s -vn -ar 48000 -ac 2 -f wav %s " + FFMPEG_QUIET;
            runFfmpegSubprocess(String.format(fmt, source, destination), destination);
        }

        private void runYTDLtoFfmpegToConverter(String url, File o) {
            String destination = o.getAbsolutePath();
            String fmt = "youtube-dl -q -f bestaudio -o - \"%s\"" +
                    " | ffmpeg -y -i pipe: -vn -ar 48000 -ac 2 -f wav \"%s\" " + FFMPEG_QUIET
                    + " > /dev/null 2>&1"; // Ignore everything from stdout & stderr

            runFfmpegSubprocess(String.format(fmt, url, destination), destination);
        }

        private synchronized void runFfmpegSubprocess(final String bash, final String pipe) {
            dropFfmpeg();

            new Thread() {

                @Override
                public void run() {
                    try {
                        String guard = "{ %s ; } || { echo -n > \"%2$s\" ; rm \"%2$s\" ; }";
                        Process p = bashExec(String.format(guard, bash, pipe));
                        ffmpegProcess = p;

                        int rz = p.waitFor();
                        if (rz != 0) {
                            if (rz == 143) { // Why 143?! Assumption: force-close exit code
                                Log.d("ffmpeg subprocess ended with error code 143");
                                skip();
                            } else {
                                Log.e(new IOException("Err " + rz + " | " + StreamUtils.convertStreamToString(p.getErrorStream())));
                            }
                        }
                    } catch (Exception e) {
                        onPlaybackError(e);
                    }
                }
            }.start();

        }

        private Process bashExec(String bash) throws IOException {
            return Runtime.getRuntime().exec(new String[]{"/bin/bash", "-e", "-c", bash});
        }

        /**
         * You cannot just use `AudioInputStream getAudioInputStream(File file)` on a WAV-file -- you'll get exception
         * like `Resetting to invalid mark`.
         * <p>
         * Instead, you have to write FileInputStream to BufferedInputStream and only then pass it to AudioSystem.
         * <p>
         * https://stackoverflow.com/questions/5529754/java-io-ioexception-mark-reset-not-supported/5529906#5529906
         */
        private AudioInputStream getProperAudioInputStream(File o) throws IOException, UnsupportedAudioFileException {
            FileInputStream fis = new FileInputStream(o);
            InputStream bufferedIn = new BufferedInputStream(fis);
            return AudioSystem.getAudioInputStream(bufferedIn);
        }

        synchronized void dropFfmpeg() {
            if (ffmpegProcess != null) {
                ffmpegProcess.destroy();
                ffmpegProcess = null;
            }
        }


        // API

        public boolean isPlaying() {
            return playing;
        }

        public void appendToPlaylist(MediaResource r) {
            playbackQueue.add(r);

            if (!isPlaying()) goNext();
        }

        public MediaResource current() {
            return playing ? playbackCurrent : null;
        }

        public MediaResource[] queue() {
            return playbackQueue.toArray(new MediaResource[0]);
        }

        public void setListener(HPStateChangeListener l) {
            this.listener = l;
        }

        /**
         * Proceed to the next item in playlist
         */
        public void skip() {
            markNotPlaying();
            voice.stopPlayback();
            goNext();
        }

        /**
         * Clears playlist and skips currently playing stream
         */
        public void stop() {
            playbackQueue.clear();
            skip();
        }
    }
}
