package com.somobu.kurabu.dc;

import com.iillyyaa2033.discord.v8.gateway_objs.MessageDeleted;
import com.iillyyaa2033.discord.v8.objects.*;
import com.somobu.kurabu.Task;

import java.io.IOException;

public class DiscordTask extends Task {

    public String guildId;

    // ============ Scheduled tasks ============ //

    @Override
    public final void onScheduled(Object[] data) throws IOException {
        onScheduled((DiscordConnection) data[0], (DiscordServer) data[1]);
    }

    public void onScheduled(DiscordConnection connection, DiscordServer server) throws IOException {

    }


    // ========== Realtime api events ========== //

    /**
     * Override and set to true to receive bot messages in onMessage
     */
    public boolean handleBotMessages() {
        return false;
    }

    public void onMessage(DiscordConnection connection, DiscordServer server, Message message) throws IOException {

    }

    public void onMessageDelete(DiscordConnection connection, DiscordServer server, MessageDeleted message) throws IOException {

    }

    public void onVoiceStateUpdate(DiscordConnection connection, DiscordServer server, VoiceState state) {

    }

    // ======== Application commands api ======== //

    public ApplicationCommand getApplicationCommand() {
        return null;
    }

    public InteractionResponse respondToInteraction(DiscordConnection connection, Interaction event) {
        return null;
    }

    public final InteractionResponse r(String content) {
        InteractionResponse response = new InteractionResponse();
        response.type = 4;
        response.data = new InteractionCallbackData();
        response.data.content = content;

        return response;
    }
}
