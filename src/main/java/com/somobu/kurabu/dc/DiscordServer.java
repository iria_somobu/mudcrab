package com.somobu.kurabu.dc;

import com.iillyyaa2033.utils.StreamUtils;
import com.somobu.kurabu.Cfg;
import com.somobu.kurabu.Main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class DiscordServer {

    @SuppressWarnings("unused")
    public String name = "Unnamed server";

    /**
     * Config version for migration purposes
     */
    @SuppressWarnings("unused")
    public int v = 4;

    /**
     * Do not mess with discord slash commands. Useful mainly for debugging purposes
     */
    public boolean suppress_cmd_loading = false;

    /**
     * На этом канале будет происходить спам юзеро-понятными сообщениями
     */
    public String main_channel = null;

    /**
     * На этом канале будет происходить спам около-системными сообщениями (типа модерации)
     */
    public String sys_channel = null;

    public Cfg.Task[] tasks = {};

    public transient String id = "";
    public transient ArrayList<DiscordTask> actual_tasks = new ArrayList<>();

    public static DiscordServer fromFile(File file) throws FileNotFoundException {
        return Main.gson.fromJson(StreamUtils.convertStreamToString(new FileInputStream(file)), DiscordServer.class);
    }
}
