package com.somobu.kurabu.irc;

import com.somobu.kurabu.Main;
import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.PircBot;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class IrcConnection {

    public ArrayList<IrcTask> tasks = new ArrayList<>();
    public PircBot bot = new MyPircBot();

    public void restart() throws IOException, IrcException, NoSuchFieldException, IllegalAccessException {

        Field field = PircBot.class.getDeclaredField("_name");
        field.setAccessible(true);
        field.set(bot, Main.cfg.irc.login);

        bot.changeNick(Main.cfg.irc.login);
        bot.setEncoding("UTF-8");
        bot.connect(Main.cfg.irc.ip, Main.cfg.irc.port, Main.cfg.irc.password);

    }

    private class MyPircBot extends PircBot {

        private final ArrayBlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(128);
        private final ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 4, 2, TimeUnit.MINUTES, queue);
        private final long startTime = System.currentTimeMillis();

        @Override
        protected void onConnect() {
            Main.d("mudcrab-bridge -- Connected to lojkka irc");
            bot.joinChannel("#lojkka");
        }

        @Override
        protected void onDisconnect() {
            try {
                restart();
            } catch (IOException | IrcException | NoSuchFieldException | IllegalAccessException e) {
                Main.e(e);
            }
        }

        @Override
        protected void onJoin(final String channel, final String sender, final String login, final String hostname) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    boolean isMe = Main.cfg.irc.login.equals(sender);
                    boolean firstSeconds = System.currentTimeMillis() - startTime < 5 * 1000;
                    if (isMe && firstSeconds) return;

                    for (IrcTask task : tasks) task.onUserJoin(channel, login, hostname);
                }
            });
        }

        @Override
        protected void onQuit(String sourceNick, final String sourceLogin, final String sourceHostname, final String reason) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    for (IrcTask task : tasks) task.onUserLeft(sourceLogin, sourceHostname, reason);
                }
            });
        }

        @Override
        protected void onMessage(final String channel, String sender, final String login, final String hostname, final String message) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    for (IrcTask task : tasks) task.onUserMessage(channel, login, hostname, message);
                }
            });
        }
    }
}
