package com.somobu.kurabu;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class Task {

    // ============ Task parameters ============ //

    private Map<String, JsonElement> parameters = null;

    public void set(String key, String value) {
        if (parameters == null) parameters = new HashMap<>();
        parameters.put(key, new JsonPrimitive(value));
    }

    public void set(String key, JsonElement value) {
        if (parameters == null) parameters = new HashMap<>();
        parameters.put(key, value);
    }

    public String getString(String key) {
        JsonElement element = parameters.get(key);
        return element == null ? null : element.getAsString();
    }

    public JsonElement get(String key) {
        return parameters.get(key);
    }

    public <T> T fromJson(String key, Class<T> classOfT) {
        JsonElement element = parameters.get(key);
        return element == null ? null : Main.gson.fromJson(element, classOfT);
    }


    // ============ Scheduled tasks ============ //

    public long getLaunchTimeAfter(DiscordConnection connection, DiscordServer server, long millis) {
        return 0;
    }

    /**
     * This method should be overridden by successor class (like {@link DiscordTask})
     * that'll provide its own `onScheduled` method to override.
     *
     * @param data should be converted to actual parameters by successors
     */
    public abstract void onScheduled(Object[] data) throws IOException;
}
