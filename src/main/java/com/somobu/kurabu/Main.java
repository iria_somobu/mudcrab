package com.somobu.kurabu;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.iillyyaa2033.utils.StreamUtils;
import com.somobu.Utils;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.irc.IrcConnection;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Modifier;

public class Main {

    public static Gson gson;

    public static String RES = "";
    public static Cfg.BotConfig cfg;

    public static DB db;

    public static DiscordConnection discord = null;
    public static IrcConnection irc = null;

    public static Necron necron;

    public static void main(String... args) throws Exception {
        System.out.println("Started on " + System.getProperty("java.version"));

        Utils.allowHttpMethod("PATCH", "PUT");

        gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.TRANSIENT)
                .create();

        RES = args[0];
        db = new DB(RES + "/db/database");
        System.load(RES + "/libs/linux-x86-64/libjnopus.so");

        File cfgFile = new File(RES, "/../cfgs/config_v2.json");
        cfg = gson.fromJson(StreamUtils.convertStreamToString(new FileInputStream(cfgFile)), Cfg.BotConfig.class);

        if (cfg.irc != null) {
            irc = new IrcConnection();
            Cfg.instantiateTasks(cfg.irc.tasks, irc.tasks);
            irc.restart();
        }

        if (cfg.discord != null) {
            discord = new DiscordConnection(cfg.discord.token);
            discord.loadConfigs();
            discord.reloadCommands();
            discord.start();
        }

        Thread.sleep(2 * 1000);

        necron = new Necron();
        necron.start();

        d(cfg.instance_name + " instance is now up and running");
        Thread.sleep(Long.MAX_VALUE);
    }

    public static void d(String msg) {
        try {
            if (discord != null && cfg.discord.system_log_channel != null) {
                discord.postMessage(cfg.discord.system_log_channel, msg);
            } else {
                System.out.println("" + msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void e(Exception e) {
        String output = "```\n" + e.getMessage();
        for (StackTraceElement el : e.getStackTrace()) output += "\n\t" + el.toString();
        output += "\n```";
        e.printStackTrace();
        d(output);
    }
}
