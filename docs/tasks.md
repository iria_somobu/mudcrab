# Описание задач бота

## Задачи-реакции

Вид задач, при выполнении которых бот ищет ключевое слово сообщении пользователя и отвечает заданным сообщением в тот же канал.

| Задача | Описание
| --- | ---
| .AwooJob | Реакция на сообщение, начинающееся с `awo`: бот постит соответствующую пикчу.
| .IgnoreMudcrabJob | Запрещает боту реагировать на себя самого.
| .MudcrabMentionedJob | Реакция на пинг бота.
| .PadoruJob | В декабре бот будет постить соответствующую пикчу, если в сообщении встречается `падору`
| .lojkka.KotlovaMentionedJob | Реакция на упоминание Козловой или Котловой.
| .maremir.CornelJob | Бот реагирует на сообщения больше определенной длинны (1200 символов).
| .maremir.RrruuuJob | Реакция на `рррууу` (где `р` и `у` может быть любое число >2, главное чтобы подряд).


## Задачи по времени

| Задача | Описание 
| --- | ---
| .DudesWednesday | Постит в системном канале жабу It's Wednesday Dudes по средам
| .DudesFriday | Постит в системном канале пятничную Momiji Inubashiri по пятницам
| .maremir.NicknameSaturday | Меняет никнейм бота в ночь с пятницы на субботу
| .maremir.NicknameSaturday | Меняет никнейм бота в ночь с воскресенья на понедельник
| .maremir.RandomMorrowind | Раз в случайные \[48;64\] часа вкидывает случайную фразу из Морровинда или Тук-Тук-Тук в системный канал.
| .rss.PanoramaNews | Раз в час вкидывает новые статьи ИА Панорама в системный канал.

*N.B.* здесь "системный канал" - это канал, указанный в поле "necron_channel" конфига сервера.


## .BotCleanup

Команды `cleanup` и `чистка`: в канале сообщения бот удаляет из последних 50 сообщений все сообщения от ботов и все сообщения-команды (сообщения пользователей, начинающиеся с `!`, `>` или `~>`).


## .DiceRoll

Команды `dice`, `roll`, `дайс` и `ролл` для броска дайса (игральной кости, она же кубик.

Общий вид: `roll XdY`, где `x` кол-во бросков, `y` кол-во граней кубика.

Пример использования: `roll 1d20` бросает 1 кубик d20.


## .MinesweeperJob

Сапер в Дискорде командами `сапер`, `сапёр` и `minesweeper`.

Варианты:
 - `сапер`: стандартное поле 10х10 с 10-ю минами
 - `сапер X`: поле 10х10 с X минами
 - `сапер X Y`: поле XxY с 10 минами
 - `сапер X Y Z`: поле XxY с Z минами 


## .Remidme

Напоминалка по команде `remind`, `remidme` и `напомни`.

Формат `remindme DATE TEXT`. Дата задается как число + модификатор. Доступные модификаторы: `m` минуты, `h` часы, `d` дни, `M` месяцы, `y` года.

Пример использования: `remidme 1d AWAWWO~` просит бота пингануть автора через 1 сутки с указанным текстом.

N.B. `remindme 1M` != `remindme 31d`, т.к. сдвиг по месяцам (и годам) идет календарный, т.е. с сохранением числа.


## .WordStatsJob

Подсчитывает суммарное количество слов каждого пользователя.

`топ` выводит топ пользователей за день
`топ все` - топ пользователей за все время работы задачи


## .maremir.CommandsJob

Сборник команд Маречатика. Для полной инфы см.сорцы или вывод команды `команды`.


## .radio.RadioJob

Дает возможность играть музыку в войсе.

Команды:

| Команда     | Пояснение |
| ---         | --- |
| join        | Призывает бота в войс |
| leave       | Выгоняет бота из войса |
| q           | Вывести текущий плейлист |
| skip        | Перейти к следующему пункту плейлиста |
| p yt {URL/QUEUE} | Добавить в плейлист музыку из ютуб-видео по ссылке или поисковой фразе |
| p rw        | Добавить в плейлист одну мелодию с retrowave.ru |
| p rw COUNT  | Добавить в плейлист COUNT мелодий с retrowave.ru |
| p QUEUE     | *WIP: сейчас то же самое, что и p yt QUEUE* |


Параметры:

| Параметр    | Пояснение |
| ---         | --- |
| control_channel | ID дискорд-канала, из которого рулим ботом |
| music_root  | Путь к папке с музыкой на диске |

